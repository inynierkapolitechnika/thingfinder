USE [ThingFinder]
GO
/****** Object:  Trigger [dbo].[Registered]    Script Date: 03.01.2017 17:30:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[Registered]
   ON  [dbo].[AspNetUsers]
   AFTER INSERT
AS 
BEGIN
	INSERT INTO [dbo].[AspNetUserRoles] VALUES ((select top 1 Id from inserted),3);
END
