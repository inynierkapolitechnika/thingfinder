USE [ThingFinder]
GO
/****** Object:  Trigger [dbo].[SetStatus]    Script Date: 03.01.2017 17:31:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[SetStatus]
   ON  [dbo].[AspNetUsers]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Imie nvarchar(50), @Nazwisko nvarchar(50), @Telefon nvarchar(50), @Id nvarchar(MAX);
	SET @Imie = (SELECT TOP 1 Name  from inserted)
	SET @Nazwisko = (SELECT TOP 1 Surname  from inserted)
	SET @Telefon = (SELECT TOP 1 PhoneNumber  from inserted)
	SET @Id = ((SELECT TOP 1 Id  from inserted))
	IF @Imie IS NOT NULL AND @Nazwisko IS NOT NULL AND @Telefon IS NOT NULL
		BEGIN
			IF (SELECT COUNT(*) FROM [dbo].[AspNetUserRoles] WHERE UserId = @Id  AND RoleId = 2 ) > 0
			BEGIN
				 WAITFOR DELAY '00:00:00'
			END
			ELSE IF (SELECT COUNT(*) FROM [dbo].[AspNetUserRoles] WHERE UserId = @Id  AND RoleId = 1 ) > 0
			BEGIN
				 WAITFOR DELAY '00:00:00'
			END
			ELSE IF (SELECT COUNT(*) FROM [dbo].[AspNetUserRoles] WHERE UserId = @Id  AND RoleId = 3 ) > 0
			BEGIN
				DELETE FROM  [dbo].[AspNetUserRoles] WHERE UserId = @Id AND RoleId = 3
				INSERT INTO [dbo].[AspNetUserRoles] VALUES (@ID,1);
			END
		END 
    -- Insert statements for trigger here

END
