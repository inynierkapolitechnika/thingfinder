USE [ThingFinder]
GO
/****** Object:  Trigger [dbo].[ChangeStatus]    Script Date: 24.11.2016 14:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[ChangeStatus]
   ON  [dbo].[Reports]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Things
	SET StatusId = 1
	WHERE Id = (Select Top 1 ThingId  from inserted)
    -- Insert statements for trigger here

END
