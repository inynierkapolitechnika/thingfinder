namespace ThingFinder.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnnouncmentWithUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Announcements", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Announcements", new[] { "ContactId" });
            AddColumn("dbo.Announcements", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Announcements", "UserId");
            AddForeignKey("dbo.Announcements", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Announcements", "ContactId");
            DropTable("dbo.Contacts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                        Surname = c.String(nullable: false, maxLength: 30),
                        Phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Announcements", "ContactId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Announcements", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Announcements", new[] { "UserId" });
            DropColumn("dbo.Announcements", "UserId");
            CreateIndex("dbo.Announcements", "ContactId");
            AddForeignKey("dbo.Announcements", "ContactId", "dbo.Contacts", "Id", cascadeDelete: true);
        }
    }
}
