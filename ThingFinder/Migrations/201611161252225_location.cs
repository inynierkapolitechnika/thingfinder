namespace ThingFinder.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class location : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Announcements", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Announcements", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Announcements", "Latitude", c => c.Decimal(nullable: false, precision: 10, scale: 6));
            AlterColumn("dbo.Announcements", "Longitude", c => c.Decimal(nullable: false, precision: 10, scale: 6));
        }
    }
}
