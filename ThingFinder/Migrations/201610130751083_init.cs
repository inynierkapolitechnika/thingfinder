namespace ThingFinder.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Classifieds", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Classifieds", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Classifieds", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.AspNetUsers", "UserInfo_Id", "dbo.UserInfoes");
            DropForeignKey("dbo.Filters", "Color_Id", "dbo.Colors");
            DropForeignKey("dbo.Filters", "Stuff_Id", "dbo.Stuffs");
            DropForeignKey("dbo.Photos", "Thing_Id", "dbo.Things");
            DropForeignKey("dbo.Things", "Filter_Id", "dbo.Filters");
            DropForeignKey("dbo.Things", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.Things", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Reports", "Thing_Id", "dbo.Things");
            DropIndex("dbo.Categories", new[] { "Categorys_Id" });
            DropIndex("dbo.Classifieds", new[] { "Category_Id" });
            DropIndex("dbo.Classifieds", new[] { "Contact_Id" });
            DropIndex("dbo.Classifieds", new[] { "Status_Id" });
            DropIndex("dbo.Filters", new[] { "Color_Id" });
            DropIndex("dbo.Filters", new[] { "Stuff_Id" });
            DropIndex("dbo.Photos", new[] { "Thing_Id" });
            DropIndex("dbo.Things", new[] { "Filter_Id" });
            DropIndex("dbo.Things", new[] { "Status_Id" });
            DropIndex("dbo.Things", new[] { "User_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "UserInfo_Id" });
            DropIndex("dbo.Reports", new[] { "Thing_Id" });
            RenameColumn(table: "dbo.Filters", name: "Color_Id", newName: "ColorId");
            RenameColumn(table: "dbo.Filters", name: "Stuff_Id", newName: "StuffId");
            RenameColumn(table: "dbo.Photos", name: "Thing_Id", newName: "ThingId");
            RenameColumn(table: "dbo.Things", name: "Filter_Id", newName: "FilterId");
            RenameColumn(table: "dbo.Things", name: "Status_Id", newName: "StatusId");
            RenameColumn(table: "dbo.Things", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.Reports", name: "Thing_Id", newName: "ThingId");
            RenameColumn(table: "dbo.Reports", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.Categories", name: "Categorys_Id", newName: "CategoryId");
            RenameIndex(table: "dbo.Reports", name: "IX_User_Id", newName: "IX_UserId");
            CreateTable(
                "dbo.Announcements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 40),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Keywords = c.String(maxLength: 100),
                        DateAnnouncement = c.DateTime(nullable: false),
                        Range = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        StatusId = c.Int(nullable: false),
                        ContactId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusId, cascadeDelete: true)
                .ForeignKey("dbo.Types", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.TypeId)
                .Index(t => t.CategoryId)
                .Index(t => t.StatusId)
                .Index(t => t.ContactId);
            
            AddColumn("dbo.Stuffs", "NameStuff", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "Surname", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.AspNetUsers", "City", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.AspNetUsers", "Street", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.AspNetUsers", "NumberFlats", c => c.String(nullable: false, maxLength: 5));
            AddColumn("dbo.AspNetUsers", "ZipCode", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Categories", "CategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Contacts", "Name", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Contacts", "Surname", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Contacts", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.Status", "NameStatus", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Colors", "NameColor", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Filters", "ColorId", c => c.Int(nullable: false));
            AlterColumn("dbo.Filters", "StuffId", c => c.Int(nullable: false));
            AlterColumn("dbo.Photos", "Icon", c => c.Binary(nullable: false));
            AlterColumn("dbo.Photos", "Picture", c => c.Binary(nullable: false));
            AlterColumn("dbo.Photos", "ThingId", c => c.Int(nullable: false));
            AlterColumn("dbo.Things", "Code", c => c.String(nullable: false, maxLength: 9));
            AlterColumn("dbo.Things", "Description", c => c.String(nullable: false, maxLength: 70));
            AlterColumn("dbo.Things", "FilterId", c => c.Int(nullable: false));
            AlterColumn("dbo.Things", "StatusId", c => c.Int(nullable: false));
            AlterColumn("dbo.Things", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Reports", "Description", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Reports", "Finder", c => c.String(maxLength: 60));
            AlterColumn("dbo.Reports", "ThingId", c => c.Int(nullable: false));
            AlterColumn("dbo.Types", "Name", c => c.String(nullable: false, maxLength: 20));
            CreateIndex("dbo.Categories", "Name", unique: true);
            CreateIndex("dbo.Categories", "CategoryId");
            CreateIndex("dbo.Things", "UserId");
            CreateIndex("dbo.Things", "FilterId");
            CreateIndex("dbo.Things", "StatusId");
            CreateIndex("dbo.Filters", "ColorId");
            CreateIndex("dbo.Filters", "StuffId");
            CreateIndex("dbo.Colors", "NameColor", unique: true);
            CreateIndex("dbo.Photos", "ThingId");
            CreateIndex("dbo.Reports", "ThingId");
            AddForeignKey("dbo.Filters", "ColorId", "dbo.Colors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Filters", "StuffId", "dbo.Stuffs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Photos", "ThingId", "dbo.Things", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Things", "FilterId", "dbo.Filters", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Things", "StatusId", "dbo.Status", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Things", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reports", "ThingId", "dbo.Things", "Id", cascadeDelete: true);
            DropColumn("dbo.Categories", "Code");
            DropColumn("dbo.Stuffs", "Description");
            DropColumn("dbo.Photos", "Short");
            DropColumn("dbo.Photos", "Description");
            DropColumn("dbo.AspNetUsers", "UserInfo_Id");
            DropTable("dbo.Classifieds");
            DropTable("dbo.UserInfoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        City = c.String(),
                        Street = c.String(),
                        NumberFlats = c.String(),
                        ZipCode = c.String(),
                        Phone = c.String(),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Classifieds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Keywords = c.String(),
                        DateFind = c.DateTime(nullable: false),
                        Range = c.Int(nullable: false),
                        Category_Id = c.Int(),
                        Contact_Id = c.Int(),
                        Status_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "UserInfo_Id", c => c.Int());
            AddColumn("dbo.Photos", "Description", c => c.String());
            AddColumn("dbo.Photos", "Short", c => c.String());
            AddColumn("dbo.Stuffs", "Description", c => c.String());
            AddColumn("dbo.Categories", "Code", c => c.String());
            DropForeignKey("dbo.Reports", "ThingId", "dbo.Things");
            DropForeignKey("dbo.Things", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Things", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Things", "FilterId", "dbo.Filters");
            DropForeignKey("dbo.Photos", "ThingId", "dbo.Things");
            DropForeignKey("dbo.Filters", "StuffId", "dbo.Stuffs");
            DropForeignKey("dbo.Filters", "ColorId", "dbo.Colors");
            DropForeignKey("dbo.Announcements", "TypeId", "dbo.Types");
            DropForeignKey("dbo.Announcements", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Announcements", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Announcements", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Reports", new[] { "ThingId" });
            DropIndex("dbo.Photos", new[] { "ThingId" });
            DropIndex("dbo.Colors", new[] { "NameColor" });
            DropIndex("dbo.Filters", new[] { "StuffId" });
            DropIndex("dbo.Filters", new[] { "ColorId" });
            DropIndex("dbo.Things", new[] { "StatusId" });
            DropIndex("dbo.Things", new[] { "FilterId" });
            DropIndex("dbo.Things", new[] { "UserId" });
            DropIndex("dbo.Categories", new[] { "CategoryId" });
            DropIndex("dbo.Categories", new[] { "Name" });
            DropIndex("dbo.Announcements", new[] { "ContactId" });
            DropIndex("dbo.Announcements", new[] { "StatusId" });
            DropIndex("dbo.Announcements", new[] { "CategoryId" });
            DropIndex("dbo.Announcements", new[] { "TypeId" });
            AlterColumn("dbo.Types", "Name", c => c.String());
            AlterColumn("dbo.Reports", "ThingId", c => c.Int());
            AlterColumn("dbo.Reports", "Finder", c => c.String());
            AlterColumn("dbo.Reports", "Description", c => c.String());
            AlterColumn("dbo.Things", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Things", "StatusId", c => c.Int());
            AlterColumn("dbo.Things", "FilterId", c => c.Int());
            AlterColumn("dbo.Things", "Description", c => c.String());
            AlterColumn("dbo.Things", "Code", c => c.String());
            AlterColumn("dbo.Photos", "ThingId", c => c.Int());
            AlterColumn("dbo.Photos", "Picture", c => c.Byte(nullable: false));
            AlterColumn("dbo.Photos", "Icon", c => c.Byte(nullable: false));
            AlterColumn("dbo.Filters", "StuffId", c => c.Int());
            AlterColumn("dbo.Filters", "ColorId", c => c.Int());
            AlterColumn("dbo.Colors", "NameColor", c => c.String());
            AlterColumn("dbo.Status", "NameStatus", c => c.String());
            AlterColumn("dbo.Contacts", "Phone", c => c.String());
            AlterColumn("dbo.Contacts", "Surname", c => c.String());
            AlterColumn("dbo.Contacts", "Name", c => c.String());
            AlterColumn("dbo.Categories", "CategoryId", c => c.Int());
            AlterColumn("dbo.Categories", "Name", c => c.String());
            DropColumn("dbo.AspNetUsers", "ZipCode");
            DropColumn("dbo.AspNetUsers", "NumberFlats");
            DropColumn("dbo.AspNetUsers", "Street");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "Surname");
            DropColumn("dbo.AspNetUsers", "Name");
            DropColumn("dbo.Stuffs", "NameStuff");
            DropTable("dbo.Announcements");
            RenameIndex(table: "dbo.Reports", name: "IX_UserId", newName: "IX_User_Id");
            RenameColumn(table: "dbo.Categories", name: "CategoryId", newName: "Categorys_Id");
            RenameColumn(table: "dbo.Reports", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.Reports", name: "ThingId", newName: "Thing_Id");
            RenameColumn(table: "dbo.Things", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.Things", name: "StatusId", newName: "Status_Id");
            RenameColumn(table: "dbo.Things", name: "FilterId", newName: "Filter_Id");
            RenameColumn(table: "dbo.Photos", name: "ThingId", newName: "Thing_Id");
            RenameColumn(table: "dbo.Filters", name: "StuffId", newName: "Stuff_Id");
            RenameColumn(table: "dbo.Filters", name: "ColorId", newName: "Color_Id");
            CreateIndex("dbo.Reports", "Thing_Id");
            CreateIndex("dbo.AspNetUsers", "UserInfo_Id");
            CreateIndex("dbo.Things", "User_Id");
            CreateIndex("dbo.Things", "Status_Id");
            CreateIndex("dbo.Things", "Filter_Id");
            CreateIndex("dbo.Photos", "Thing_Id");
            CreateIndex("dbo.Filters", "Stuff_Id");
            CreateIndex("dbo.Filters", "Color_Id");
            CreateIndex("dbo.Classifieds", "Status_Id");
            CreateIndex("dbo.Classifieds", "Contact_Id");
            CreateIndex("dbo.Classifieds", "Category_Id");
            CreateIndex("dbo.Categories", "Categorys_Id");
            AddForeignKey("dbo.Reports", "Thing_Id", "dbo.Things", "Id");
            AddForeignKey("dbo.Things", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Things", "Status_Id", "dbo.Status", "Id");
            AddForeignKey("dbo.Things", "Filter_Id", "dbo.Filters", "Id");
            AddForeignKey("dbo.Photos", "Thing_Id", "dbo.Things", "Id");
            AddForeignKey("dbo.Filters", "Stuff_Id", "dbo.Stuffs", "Id");
            AddForeignKey("dbo.Filters", "Color_Id", "dbo.Colors", "Id");
            AddForeignKey("dbo.AspNetUsers", "UserInfo_Id", "dbo.UserInfoes", "Id");
            AddForeignKey("dbo.Classifieds", "Status_Id", "dbo.Status", "Id");
            AddForeignKey("dbo.Classifieds", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.Classifieds", "Category_Id", "dbo.Categories", "Id");
        }
    }
}
