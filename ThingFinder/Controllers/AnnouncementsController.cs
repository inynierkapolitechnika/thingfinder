﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ThingFinder.Models;
using ThingFinder.Models.ViewModels;
using ThingFinder.Validation;

namespace ThingFinder.Controllers
{
    public class AnnouncementsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        #region UserMananger
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion UserManager

        [ChildActionOnly]
        public ActionResult SearchAnnouncements(string id = "")
        {
            if (id.Equals("Code"))
            {
                return PartialView("~/Views/Things/_SearchCodeThing.cshtml", new SearchCodeThingViewModels { Code = null });
            }
            else if (id.Equals("Photo"))
            {
                return PartialView("~/Views/Photos/_SearchPhoto.cshtml");
            }

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new CheckBoxSearch { Text = t.Name, Value = t.Id }).ToList();

            return PartialView("_SearchAnnouncements", new SearchAnnouncementsViewModels { Categorys = categoryOptions, Types = typeOptions });
        }

        private bool CheckLocation(GeoCoordinate sCoord, GeoCoordinate eCoord, double range)
        {
            double distance = sCoord.GetDistanceTo(eCoord);
            if (range < distance)
                return false;
            return true;
        }

        // GET: Announcements
        public ActionResult Index(string keyWords = null, string longitude = null, string latitude = null, string range = null,
            string categoryId = null)
        {
            List<Announcement> announcement;

            if (!string.IsNullOrEmpty(keyWords))
            {
                announcement = db.Announcement.Where(a => a.KeyWords.Contains(keyWords) && (a.Status.Name == "Znaleziony" || a.Status.Name == "Zgubiony")).ToList();
            }
            else
            {
                announcement = db.Announcement.Where(a => a.Status.Name == "Znaleziony" || a.Status.Name == "Zgubiony").ToList();
            }

            if (!string.IsNullOrEmpty(categoryId))
            {
                announcement.RemoveAll(x => x.CategoryId != Convert.ToInt16(categoryId));
            }

            if (!string.IsNullOrEmpty(longitude) && !string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(range))
            {
                double lati = Convert.ToDouble(latitude);
                double longi = Convert.ToDouble(longitude);
                double ra = Convert.ToDouble(range);
                var sCoord = new GeoCoordinate(lati, longi);

                announcement.RemoveAll(x => !CheckLocation(sCoord, 
                    new GeoCoordinate(Convert.ToDouble(x.Latitude), Convert.ToDouble(x.Longitude)),
                    ra + Convert.ToDouble(x.Range)));
            }

            return View(announcement);
        }

        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon.")]
        public ActionResult MyAnnoucements()
        {
            var myAnnouncement = db.Announcement.Where(a => a.Status.Name == "Znaleziony" || a.Status.Name == "Zgubiony").Where(u => u.User.Email == User.Identity.Name);
            return View(myAnnouncement.ToList());
        }

        [Authorize(Roles ="Admin")]
        public ActionResult AllAnnoucements()
        {
            var announcement = db.Announcement.Include(a => a.Category).Include(a => a.Status).Include(a => a.Type);
            return View(announcement.ToList());
        }

        [ChildActionOnly]
        public ActionResult SelectedTwoLast()
        {
            var announcement = db.Announcement.Where(a => a.Status.Name == "Znaleziony" || a.Status.Name == "Zgubiony").OrderByDescending(a => a.DateOfAnnouncement).Take(2);
            return PartialView("_Announcement", announcement.ToList());
        }

        // GET: Announcements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = db.Announcement.Find(id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            return View(announcement);
        }



        // GET: Announcements/Create
        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
        public ActionResult Create()
        {

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();

            return View(new AddAnnouncementsViewModels { Categorys = categoryOptions, Status = statusOptions, Types = typeOptions });
        }

        // POST: Announcements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddAnnouncementsViewModels model)
        {

            if (ModelState.IsValid)
            {
                var announcement = new Announcement
                {
                    Description = model.Description,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    KeyWords = model.KeyWords,
                    DateOfAnnouncement = model.DateOfAnnouncement,
                    Range = model.Range,
                    TypeId = model.TypeId,
                    StatusId = model.StatusId,
                    CategoryId = model.CategoryId,
                    UserId = db.Users.First(x => x.Email == User.Identity.Name).Id
                };

                db.Announcement.Add(announcement);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw;
                }

                return RedirectToAction("Index");
            }

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();

            return View(new AddAnnouncementsViewModels { Categorys = categoryOptions, Status = statusOptions, Types = typeOptions });
        }

        #region AddAnnouncementThroughThing
        [Authorize]
        public ActionResult AddAnnoucement(int thingId)
        {
            //Czy przedmiot istnieje i nalezy do uzytkownika
            if (!db.Thing.Any(x => x.Id == thingId) && db.Thing.First(z => z.Id == thingId).User.Email != User.Identity.Name)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Thing thing = db.Thing.First(x => x.Id == thingId);

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();
            AddAnnouncementThing announcement = new AddAnnouncementThing()
            {
                Description = thing.Description,
                StatusId = thing.StatusId,
                TypeId = 1,
                CategoryId = thing.CategoryId,
                Categorys = categoryOptions,
                Status = statusOptions,
                Types = typeOptions
            }; 
            return View(announcement);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAnnoucement(AddAnnouncementThing model)
        {

            if (ModelState.IsValid)
            {
                var announcement = new Announcement
                {
                    Description = model.Description,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    KeyWords = model.KeyWords,
                    DateOfAnnouncement = model.DateOfAnnouncement,
                    Range = model.Range,
                    TypeId = model.TypeId,
                    StatusId = model.StatusId,
                    CategoryId = model.CategoryId,
                    UserId = db.Users.First(x => x.Email == User.Identity.Name).Id
                };

                db.Announcement.Add(announcement);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                return RedirectToAction("Index");
            }

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();

            return View(new AddAnnouncementThing
            {
                Description = model.Description,
                StatusId = model.StatusId,
                TypeId = 1,
                CategoryId = model.CategoryId,
                Categorys = categoryOptions, Status = statusOptions, Types = typeOptions
            });
        
    }

        #endregion AddAnnouncementThroughThing
        #region SendMessage
        [Authorize]
        public ActionResult SendMessage(int announcementId)
        {
            SendMessageViewModel model = new SendMessageViewModel();
            SendMessageViewModel.announcementId = announcementId;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> SendMessage(SendMessageViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userRecivedId = db.Announcement.First(a => a.Id == SendMessageViewModel.announcementId).UserId;
                    await UserManager.SendEmailAsync(userRecivedId, "Odpowiedź na ogłoszenie", String.Format("Ogłoszenie id: {0}({3})) \nUżytkownik: {1}\nTreść: {2}", 
                        SendMessageViewModel.announcementId, User.Identity.Name, model.Message,
                        Url.Action("Details", "Announcements" ,new{ id = SendMessageViewModel.announcementId }, Request.Url.Scheme)));
                    return RedirectToAction("Details", new { id = SendMessageViewModel.announcementId });
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return View(model);
        }
        #endregion SendMessage
        // GET: Announcements/Edit/5
        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = db.Announcement.Find(id);
            if (announcement == null)
            {
                return HttpNotFound();
            }

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();

            var model = new EditAnnouncementsViewModels
            {
                Categorys = categoryOptions,
                Status = statusOptions,
                Types = typeOptions
            };

            EditAnnouncementsViewModels.Id = announcement.Id;
            model.Description = announcement.Description;
            model.Longitude = announcement.Longitude;
            model.Latitude = announcement.Latitude;
            model.KeyWords = announcement.KeyWords;
            model.DateOfAnnouncement = announcement.DateOfAnnouncement;
            model.Range = announcement.Range;
            model.TypeId = announcement.TypeId;
            model.StatusId = announcement.StatusId;
            model.CategoryId = announcement.CategoryId;
            return View(model);
        }

        // POST: Announcements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditAnnouncementsViewModels model)
        {
            if (ModelState.IsValid)
            {
                Announcement announcement = db.Announcement.First(a => a.Id == EditAnnouncementsViewModels.Id);
                announcement.Description = model.Description;
                announcement.Latitude = model.Latitude;
                announcement.Longitude = model.Longitude;
                announcement.KeyWords = model.KeyWords;
                announcement.DateOfAnnouncement = model.DateOfAnnouncement;
                announcement.Range = model.Range;
                announcement.TypeId = model.TypeId;
                announcement.StatusId = model.StatusId;
                announcement.CategoryId = model.CategoryId;
                //announcement.UserId = db.Users.First(x => x.Email == User.Identity.Name).Id;
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException)
                {
                    throw;
                }

                return RedirectToAction("Index");
            }

            var categoryOptions = db.Category.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() }).ToList();
            var statusOptions = db.Status.Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            var typeOptions = db.Type.Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }).ToList();

            return View(new EditAnnouncementsViewModels { Categorys = categoryOptions, Status = statusOptions, Types = typeOptions });
        }

        // GET: Announcements/Delete/5
        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = db.Announcement.Find(id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            return View(announcement);
        }

        // POST: Announcements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Announcement announcement = db.Announcement.Find(id);
            db.Announcement.Remove(announcement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
