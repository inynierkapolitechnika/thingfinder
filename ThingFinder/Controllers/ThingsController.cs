﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ThingFinder.Models;
using ThingFinder.Models.ViewModels;
using ThingFinder.Validation;

namespace ThingFinder.Controllers
{
    [Authorize]
    [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
    public class ThingsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string regExp = "[a-zA-Z]{9}";


        // GET: Things
        public ActionResult Index()
        {
            string UserId = db.Users.First(x => x.Email == User.Identity.Name).Id;
            var thing = db.Thing.Include(t => t.Filter).Include(t => t.Status).Include(t => t.User).Where(x => x.User.Id == UserId);
            return View(thing.ToList());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AllThings()
        {
            var thing = db.Thing.Include(t => t.Filter).Include(t => t.Status).Include(t => t.User);
            return View(thing.ToList());
        }

        // GET: Things/Details/5
        public ActionResult Details(int? id)
        {
      
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thing thing = db.Thing.Find(id);
            if (thing == null)
            {
                return HttpNotFound();
            }
            return View(thing);
        }

        // GET: Things/Create
        public ActionResult Create()
        {
            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name");
            ViewBag.StuffId = new SelectList(db.Stuff, "Id", "Name");
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");

            if (!db.Users.First(x => x.Email == User.Identity.Name).CanAddThing)
            {
                TempData["MessageFromMyAttribute"] = "Proszę uzupełnić dane: Imie, Nazwisko, Telefon";
                return RedirectToAction("Settings", "Manage");
            }
              

            return View();
        }

        // POST: Things/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(RegisterThingViewModel registerdThing)
        {
            Fare.Xeger xeger = new Fare.Xeger(regExp);
            string Code = xeger.Generate();
            int exists = db.Thing.Where(x => x.Code == Code).ToList().Count;
            while (exists > 0)
            {
                Code = xeger.Generate();
                exists = db.Thing.Where(x => x.Code == Code).ToList().Count;
            }
            string UserId = UserId = db.Users.First(x => x.Email == User.Identity.Name).Id;
            int StatusId = db.Status.First(x => x.Name == "OK").Id;
            Thing thing = new Thing(Code, UserId, StatusId,registerdThing.ColorId,registerdThing.CategoryId,registerdThing.Description,registerdThing.StuffId);
            if (ModelState.IsValid)
            {
                db.Thing.Add(thing);
                try
                {
                    db.SaveChanges();

                }
                catch (Exception ex)
                {

                    throw;
                }
                
                return RedirectToAction("Index");
            }

            ViewBag.ColorId = new SelectList(db.Color, "Id", "Name");
            ViewBag.StuffId = new SelectList(db.Stuff, "Id", "Name");
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");

            return View();
        }

        // GET: Things/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thing thing = db.Thing.Find(id);
            if (thing == null)
            {
                return HttpNotFound();
            }
           // ViewBag.FilterId = new SelectList(db.Filter, "Id", "Id", thing.FilterId);
            ViewBag.StatusId = new SelectList(db.Status, "Id", "Name", thing.StatusId);
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            EditThingViewModel editthing = new EditThingViewModel();
            EditThingViewModel.ThingId = thing.Id;
            editthing.StatusId = thing.StatusId;
            editthing.CategoryId = thing.CategoryId;
            editthing.Description = thing.Description;
            return View(editthing);
        }

        // POST: Things/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditThingViewModel editthing)
        {
            if (ModelState.IsValid)
            {
                Thing thing = db.Thing.First(x => x.Id == EditThingViewModel.ThingId);
                thing.Description = editthing.Description;
                thing.CategoryId = editthing.CategoryId;
                thing.StatusId = editthing.StatusId;
                db.Entry(thing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           // ViewBag.FilterId = new SelectList(db.Filter, "Id", "Id", thing.FilterId);
            ViewBag.StatusId = new SelectList(db.Status, "Id", "Name", editthing.StatusId);
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            return View(editthing);
        }

        // GET: Things/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thing thing = db.Thing.Find(id);
            if (thing == null)
            {
                return HttpNotFound();
            }
            return View(thing);
        }

        // POST: Things/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Thing thing = db.Thing.Find(id);
            db.Thing.Remove(thing);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult GenerateSticker(string code)
        {
            string userId = db.Users.First(x => x.Email == User.Identity.Name).Id;
            if (!db.Thing.Any( x=> x.Code == code))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            Thing thing = db.Thing.First(x => x.Code == code);
            if (userId != thing.UserId)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            Image sticker = Helpers.QRCodeMaker.GenerateSticker(code,Url.Action("Found","Reports",new { code = thing.Code },Request.Url.Scheme));
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = code+".bmp",

                Inline = false,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(ThingFinder.Helpers.PhotoTools.ImageToByte(sticker), "image/bmp");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
