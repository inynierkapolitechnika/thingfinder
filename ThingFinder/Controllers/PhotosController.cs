﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ThingFinder.Models;
using ThingFinder.Models.ViewModels;

namespace ThingFinder.Controllers
{
    [Authorize]
    public class PhotosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize(Roles ="Admin")]
        // GET: Photos
        public ActionResult Index()
        {
            var photo = db.Photo.Include(p => p.Thing);
            return View(photo.ToList());
        }
        [Authorize(Roles = "Admin")]
        // GET: Photos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photo.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            return View(photo);
        }
        [Authorize]
        // GET: Photos/Create
        public ActionResult Create(int id)
        {
            var isExists = db.Thing.Any(x => x.Id == id);
            if (!isExists)
                return HttpNotFound();
            if (User.Identity.Name != db.Thing.First(x => x.Id == id).User.Email)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            AddPhotoViewModel.ThingId = id;
            //ViewBag.ThingId = new SelectList(db.Thing, "Id", "Code");
            return View(new AddPhotoViewModel());
        }

        // POST: Photos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase file)
        {
            if (file == null || !file.ContentType.Contains("image"))
            {
                ViewBag.Error = "Nie zamieszczono pliku lub plik nie jest w formacie jpg, gif, png , jpeg";
                return View(new AddPhotoViewModel());
            }
            if (ModelState.IsValid)
            {
                //tworzenie ikony
                var icon = ThingFinder.Helpers.PhotoTools.ResizeImage(Image.FromStream(file.InputStream), 100, 100);
                db.Photo.Add(new Photo { Picture = ThingFinder.Helpers.PhotoTools.ImageToByte(Image.FromStream(file.InputStream)) , ThingId = AddPhotoViewModel.ThingId, Icon = ThingFinder.Helpers.PhotoTools.ImageToByte(icon)  });
                db.SaveChanges();
                return RedirectToAction("Index","Things");
            }

            //ViewBag.ThingId = new SelectList(db.Thing, "Id", "Code", photo.ThingId);
            return View(new AddPhotoViewModel());
        }

        // GET: Photos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photo.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            ViewBag.ThingId = new SelectList(db.Thing, "Id", "Code", photo.ThingId);
            return View(photo);
        }

        // POST: Photos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Icon,Picture,ThingId")] Photo photo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(photo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ThingId = new SelectList(db.Thing, "Id", "Code", photo.ThingId);
            return View(photo);
        }

        // GET: Photos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photo.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            return View(photo);
        }

        // POST: Photos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Photo photo = db.Photo.Find(id);
            db.Photo.Remove(photo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult FindByPhoto()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FindByPhoto(AddPhotoViewModel model ,HttpPostedFileBase file)
        {
            if (file == null || !file.ContentType.Contains("image"))
            {
                ViewBag.Error = "Nie zamieszczono pliku lub plik nie jest w formacie jpg, gif, png , jpeg";
                return View();
            }
            if (ModelState.IsValid)
            {
                //tworzenie ikony
                Image uploadImage = Image.FromStream(file.InputStream);
                List<ShowSimilars> similarImages = new List<ShowSimilars>();
                foreach (var subject in db.Photo)
                {
                    Image subjectPhoto = Helpers.PhotoTools.ConvertToImage(subject.Picture);
                   
                    var percentDiffrence = XnaFan.ImageComparison.ExtensionMethods.PercentageDifference(uploadImage, subjectPhoto,10);
                    var percentDiffrenceBhattacharyya = XnaFan.ImageComparison.ExtensionMethods.BhattacharyyaDifference(uploadImage, subjectPhoto);
                    if (percentDiffrence < 0.5 || model.isBhattacharyya)
                        similarImages.Add(new Models.ViewModels.ShowSimilars() { similar = ((1-percentDiffrence)*100).ToString() + "%", Photo = subject.Icon, similarBhattacharyya = percentDiffrenceBhattacharyya.ToString() });
                }
                if (similarImages.Count == 0)
                    return Content("Brak podobnych obrazków");
                if (model.isBhattacharyya)
                    similarImages = similarImages.OrderBy(x => x.similarBhattacharyya).Take(5).ToList();
                return View("ShowSimilars", similarImages);
            }

            //ViewBag.ThingId = new SelectList(db.Thing, "Id", "Code", photo.ThingId);
            return View(new AddPhotoViewModel());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
