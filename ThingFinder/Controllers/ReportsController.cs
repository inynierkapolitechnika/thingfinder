﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ThingFinder.Models;
using ThingFinder.Models.ViewModels;
using ThingFinder.Validation;

namespace ThingFinder.Controllers
{
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;    
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Reports
        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
        public ActionResult Index()
        {
            //na szybko
            var reports = db.Report.Where(x => x.Thing.User.Email == User.Identity.Name);
            return View(reports.ToList());
        }

        // GET: Reports/Details/5
        [Authorize]
        [RoleAuthorized(Message = "Nie jesteś autoryzowany. Proszę uzupełnić dane: Imie, Nazwisko, Telefon")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Report report = db.Report.Find(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(report);
        }

        public ActionResult Found(string code)
        {
            if (db.Thing.Any(x => x.Code == code))
            {
                // await UserManager.SendEmailAsync(db.Thing.First(x => x.Code == code).UserId, "Znalazeiono Przedmiot", "Przedmiot o kodzie " + code + "został znaleziony. Szczegóły znajdują się reporcie");
                //return Content("Gratulacje znalazco");
                AddReportViewModel reportViewModel = new AddReportViewModel();
                AddReportViewModel.ThingId = db.Thing.First(x => x.Code == code).Id;
                AddReportViewModel.OwnerId = db.Thing.First(x => x.Code == code).UserId;
                AddReportViewModel.codeThing = code;
                if (Request.IsAuthenticated)
                    reportViewModel.Founder = User.Identity.Name;
                return View(reportViewModel);
            }

            else return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Found([Bind(Include = "Id,Longitude,Latitude,Description,DateOfFound,DateOfNotification,Founder,UserId,ThingId")] AddReportViewModel report)
        {
            if (ModelState.IsValid)
            {
                Report rep = new Report() { Longitude = report.Longitude, Latitude = report.Latitude, DateOfNotification = System.DateTime.Now, DateOfFound = report.DateOfFound, Description = report.Description, ThingId = AddReportViewModel.ThingId, Founder = report.Founder };
                string codeThing="";
                string UserId="";
              
                db.Report.Add(rep);
                try
                {
                    db.SaveChanges();
                    //UserId = db.Thing.First(x => x.Code == rep.Thing.Code).UserId; codeThing = rep.Thing.Code;       //nie dziala bo nie wiem dlaczego
                    UserId = AddReportViewModel.OwnerId; codeThing = AddReportViewModel.codeThing;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                await UserManager.SendEmailAsync(UserId, "Znaleziono Przedmiot", "Przedmiot o kodzie " + codeThing + " został znaleziony. Szczegóły znajdują się raporcie");
                return RedirectToAction("Index", "Home");
            }
            return View(report);
        }

        //// POST: Reports/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Longitude,Latitude,Description,DateOfFound,DateOfNotification,Founder,UserId,ThingId")] Report report)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Reports.Add(report);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.ThingId = new SelectList(db.Things, "Id", "Code", report.ThingId);
        //    ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Name", report.UserId);
        //    return View(report);
        //}

        //// GET: Reports/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Report report = db.Reports.Find(id);
        //    if (report == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ThingId = new SelectList(db.Things, "Id", "Code", report.ThingId);
        //    ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Name", report.UserId);
        //    return View(report);
        //}

        //// POST: Reports/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Longitude,Latitude,Description,DateOfFound,DateOfNotification,Founder,UserId,ThingId")] Report report)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(report).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ThingId = new SelectList(db.Things, "Id", "Code", report.ThingId);
        //    ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Name", report.UserId);
        //    return View(report);
        //}

        //// GET: Reports/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Report report = db.Reports.Find(id);
        //    if (report == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(report);
        //}

        //// POST: Reports/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Report report = db.Reports.Find(id);
        //    db.Reports.Remove(report);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
