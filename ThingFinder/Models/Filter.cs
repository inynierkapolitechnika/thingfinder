﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Filter
    {
        public int Id { get; set; }
        #region Relations
        public virtual Color Color { get; set; }
        public virtual Stuff Stuff { get; set; }
        public virtual ICollection<Thing> Thing { get; set; }
        #endregion Relations
        #region ForeignKeys
        [Required]
        [ForeignKey("Color")]
        public int ColorId { get; set; }

        [Required]
        [ForeignKey("Stuff")]
        public int StuffId { get; set; }
        #endregion ForeignKeys
    }
}