﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Status
    {
        public int Id { get; set; }
        /// <summary>
        /// Nazwa statusu
        /// </summary>
        [Required]
        [Display(Name="Status")]
        [MaxLength(30)]
        public string Name { get; set; }
        #region Relations
        public virtual ICollection<Thing> Thing { get; set; }
        public virtual ICollection<Announcement> Announcement { get; set; }
        #endregion Relations
    }
}