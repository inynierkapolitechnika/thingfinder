﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Category
    {
        public int Id { get; set; }
        /// <summary>
        /// Nazwa Kategorii
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public string Name { get; set; }
        #region Relations
        public virtual ICollection<Thing> Thing { get; set; }
        public virtual ICollection<Announcement> Announcement { get; set; }
        public virtual ICollection<Category> SubCategories { get; set; }  
        public virtual Category UpCategory { get; set; }
        #endregion Relations
        #region ForeignKey
        [ForeignKey("UpCategory")]
        public int? CategoryId { get; set; }
        #endregion ForeignKey

    }
}