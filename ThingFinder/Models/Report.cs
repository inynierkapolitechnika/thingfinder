﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ThingFinder.Attributes;

namespace ThingFinder.Models
{
    public class Report
    {
        public int Id { get; set; }
        /// <summary>
        /// Długość Geograficzna
        /// </summary>
        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Szerokość")]
        public decimal Longitude { get; set; }
        /// <summary>
        /// Szerokość Geograficzna
        /// </summary>
        [Required]
        [Precision(10, 6)]
        [Display(Name = "Długość")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        public decimal Latitude { get; set; }
        /// <summary>
        /// Opis Zgloszenia
        /// </summary>
        [Required]
        [MaxLength(100)]
        [Display(Name = "Uwagi")]
        public string Description { get; set; }
        /// <summary>
        /// Data Znalezienia
        /// </summary>
        [Required]
        [Display(Name = "Data znalezienia")]
        [DisplayFormat(    ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfFound { get; set; }
        /// <summary>
        /// Data zgłoszenia
        /// </summary>
        [Required]
        [Display(Name = "Data zgłoszenia")]
        public DateTime DateOfNotification { get; set; }
        /// <summary>
        /// Znalazca
        /// </summary>
        [MaxLength(60)]
        [Display(Name = "Znalazca")]
        public string Founder { get; set; }
        #region Relations
        public virtual Thing Thing { get; set; }
        public virtual ApplicationUser User { get; set; }
        #endregion Relations
        #region ForeignKeys
        [ForeignKey("User")]
        public string UserId { get; set; }

        [Required]
        [ForeignKey("Thing")]
        public int ThingId { get; set; }
        #endregion ForeignKeys
    }
}