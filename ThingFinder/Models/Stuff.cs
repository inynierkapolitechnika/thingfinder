﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Stuff
    {
        public int Id { get; set; }
        /// <summary>
        /// Nazwa materiału
        /// </summary>
        [Required]
        [MaxLength(30)]
        [Display(Name = "Materiał")]
        public string Name { get; set; }
        #region Relation
        public virtual ICollection<Filter> Filter { get; set; }
        #endregion Relation
    }
}