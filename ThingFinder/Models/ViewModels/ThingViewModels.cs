﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThingFinder.Models.ViewModels
{

    public class RegisterThingViewModel
    {
        [Required]
        [MaxLength(70)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }

        [Required]
        [Display(Name = "Kolor")]
        public int ColorId { get; set; }
        [Required]
        [Display(Name = "Materiał")]
        public int StuffId { get; set; }
        

    }

    public class EditThingViewModel
    {
        public static int ThingId;
        [Required]
        [MaxLength(70)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int StatusId { get; set; }
    }

    public class SearchCodeThingViewModels
    {
        [Display(Name = "Kod")]
        public string Code { get; set; }
    }
}