﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThingFinder.Models.ViewModels
{
   public class AddPhotoViewModel
    {
        public static int ThingId;
        public byte[] Picture { get; set; }
        [Display(Name = "Dystans Bhattacharyya")]
        public bool isBhattacharyya { get; set; }
    }
    public class ShowSimilars
    {
        public byte[] Photo { get; set; }
        public string similar { get; set; }
        public string similarBhattacharyya { get; set; }
    }
}