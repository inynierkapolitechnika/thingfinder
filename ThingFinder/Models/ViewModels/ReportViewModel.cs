﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ThingFinder.Attributes;
using ThingFinder.Validation;

namespace ThingFinder.Models.ViewModels
{
    public class AddReportViewModel
    {

        public static int ThingId { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [Display(Name ="Szerokość")]
        public decimal Longitude { get; set; }
        /// <summary>
        /// Szerokość Geograficzna
        /// </summary>
        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [Display(Name = "Długość")]
        public decimal Latitude { get; set; }
        /// <summary>
        /// Opis Zgloszenia
        /// </summary>
        [Required]
        [MaxLength(100)]
        [Display(Name = "Uwagi")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        /// <summary>
        /// Data Znalezienia
        /// </summary>
        [Required]
        [SqlDateTimeMinMax]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Data Znalezienia")]
        public DateTime DateOfFound { get; set; }
        /// <summary>
        /// Znalazca
        /// </summary>
        [Required]
        [MaxLength(60)]
        [EmailAddress(ErrorMessage = "To nie jest email")]
        [Display(Name = "Znalazca")]
        public string Founder { get; set; }
        public static string OwnerId { get; set; }
        public static string codeThing { get;  set; }
    }
}