﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThingFinder.Attributes;
using ThingFinder.Validation;

namespace ThingFinder.Models.ViewModels
{
    public class AddAnnouncementsViewModels
    {
        [Required]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        [Display(Name ="Opis")]
        public string Description { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Długość")]
        public decimal Longitude { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Szerokość")]
        public decimal Latitude { get; set; }

        [MaxLength(100)]
        [Display(Name = "Słowa Klucze")]
        public string KeyWords { get; set; }

        [Required]
        [SqlDateTimeMinMax]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Data Ogłoszenia")]
        public DateTime DateOfAnnouncement { get; set; }

        [Display(Name = "Zakres")]
        public int? Range { get; set; }

        [Required]
        [Display(Name = "Rodzaj")]
        public int TypeId { get; set; }
        [Display(Name = "Rodzaj")]
        public IEnumerable<SelectListItem> Types { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }
        [Display(Name = "Kategoria")]
        public IEnumerable<SelectListItem> Categorys { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int StatusId { get; set; }
        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

       
    }

    public class CheckBoxSearch
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public bool IsSelected { get; set; }
    }

    public class SearchAnnouncementsViewModels
    {

        public SearchAnnouncementsViewModels()
        {
            Types = new List<CheckBoxSearch>();
        }

        [MaxLength(100)]
        [Display(Name = "Słowa Klucze")]
        public string KeyWords { get; set; }

        [Display(Name = "Rodzaj")]
        public List<CheckBoxSearch> Types { get; set; }

        [Display(Name = "Kategoria")]
        public int? CategoryId { get; set; }
        [Display(Name = "Kategoria")]
        public IEnumerable<SelectListItem> Categorys { get; set; }

        [Precision(10, 6)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Szerokość")]
        public decimal? Longitude { get; set; }

        [Precision(10, 6)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Długość")]
        public decimal? Latitude { get; set; }

        [Display(Name = "Zakres")]
        public int? Range { get; set; }
    }

    public class EditAnnouncementsViewModels
    {
        public static int Id;

        [Required]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Szerokość")]
        public decimal Longitude { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Długość")]
        public decimal Latitude { get; set; }

        [MaxLength(100)]
        [Display(Name = "Słowa Klucze")]
        public string KeyWords { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [SqlDateTimeMinMax]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Data Ogłoszenia")]
        public DateTime DateOfAnnouncement { get; set; }

        [Display(Name = "Zakres")]
        public int? Range { get; set; }

        [Required]
        [Display(Name = "Rodzaj")]
        public int TypeId { get; set; }
        [Display(Name = "Rodzaj")]
        public IEnumerable<SelectListItem> Types { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }
        [Display(Name = "Kategoria")]
        public IEnumerable<SelectListItem> Categorys { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int StatusId { get; set; }
        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }
    }

    public class AddAnnouncementThing
    {
        [Required]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Długość")]
        public decimal Longitude { get; set; }

        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        [Display(Name = "Szerokość")]
        public decimal Latitude { get; set; }

        [MaxLength(100)]
        [Display(Name = "Słowa Klucze")]
        public string KeyWords { get; set; }

        [Required]
        [SqlDateTimeMinMax]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Data Ogłoszenia")]
        public DateTime DateOfAnnouncement
        {
            get { return dateOfAnnoucement; }
            set { dateOfAnnoucement = value; }
        }
        /// <summary>
        /// Aby ustawiało dzisiejsza date
        /// </summary>
        private DateTime dateOfAnnoucement = System.DateTime.Now;

        [Display(Name = "Zakres")]
        public int? Range { get; set; }

        [Required]
        [Display(Name = "Rodzaj")]
        public int TypeId { get; set; }
        [Display(Name = "Rodzaj")]
        public IEnumerable<SelectListItem> Types { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }
        [Display(Name = "Kategoria")]
        public IEnumerable<SelectListItem> Categorys { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int StatusId { get; set; }
        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

    }

    public class SendMessageViewModel
    {
        public static int announcementId { get; set; }

        [Required]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Wiadomość")]
        public string Message { get; set; }

    }
}