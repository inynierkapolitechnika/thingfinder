﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThingFinder.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Imie Użytkownika
        /// </summary>
        [MaxLength(30)]
        public string Name { get; set; }
        /// <summary>
        /// Nazwisko Użytkownika
        /// </summary>
        [MaxLength(30)]
        public string Surname { get; set; }
        /// <summary>
        /// Miasto
        /// </summary>
        [MaxLength(30)]
        public string City { get; set; }
        /// <summary>
        /// Ulica
        /// </summary>
        [MaxLength(30)]
        public string Street { get; set; }
        /// <summary>
        /// Numer mieszkania
        /// </summary>
        [MaxLength(5)]
        public string NumberOfFlat { get; set; }
        /// <summary>
        /// Kod pocztowy
        /// </summary>
        [StringLength(6)]
        [RegularExpression(@"[0-9]{2}-[0-9]{3}")]
        public string ZipCode { get; set; }

        public bool CanAddThing
        {
            get
            {
                return (Name != null && Surname != null) ? true : false;
            }
        }
        #region Relations
        public virtual ICollection<Report> Report { get; set; }
        public virtual ICollection<Thing> Thing { get; set; }
        public virtual ICollection<Announcement> Announcement{ get; set; }
        #endregion Relations

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Attributes.Precision.ConfigureModelBuilder(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        

        public DbSet<Category> Category { get; set; }
        public DbSet<Announcement> Announcement { get; set; }
        public DbSet<Color> Color { get; set; }
        public DbSet<Filter> Filter { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<Report> Report { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Stuff> Stuff { get; set; }
        public DbSet<Thing> Thing { get; set; }
        public DbSet<Type> Type { get; set; }
    }
}