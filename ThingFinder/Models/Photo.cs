﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Photo
    {
        public int Id { get; set; }
        /// <summary>
        /// Ikonka
        /// </summary>
        [Required]
        public byte[] Icon { get; set; }
        /// <summary>
        /// Zdjecie predmiotu
        /// </summary>
        [Required]
        public byte[] Picture { get; set; }
        #region Relation
        public virtual Thing Thing { get; set; }
        #endregion Relation
        #region ForeignKey
        [Required]
        [ForeignKey("Thing")]
        public int ThingId { get; set; }
        #endregion ForeignKey
    }
}