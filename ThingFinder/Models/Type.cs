﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Type
    {
        public int Id { get; set; }
        /// <summary>
        /// Nazwa Typu Ogłoszenia
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        #region Relation
        public virtual ICollection<Announcement> Announcement { get; set; }
        #endregion Relation
    }
}