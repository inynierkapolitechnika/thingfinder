﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ThingFinder.Attributes;

namespace ThingFinder.Models
{
    public class Announcement
    {
        public int Id { get; set; }
        /// <summary>
        /// Opis Przedmiotu
        /// </summary>
        [Required]
        [MaxLength(500)]
        public string Description { get; set; }
        /// <summary>
        /// Dlugość geograficzna
        /// </summary>
        [Required]
        [Precision(10,6)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        public decimal Longitude { get; set; }
        /// <summary>
        /// Szerokość geograficzna
        /// </summary>
        [Required]
        [Precision(10, 6)]
        [DisplayFormat(DataFormatString = "{0:n6}")]
        [RegularExpression(@"^\d{0,3}[,]\d{6}$")]
        public decimal Latitude { get; set; }
        /// <summary>
        /// Słowa Klucze
        /// </summary>
        [MaxLength(100)]
        public string KeyWords { get; set; }
        /// <summary>
        /// Data ogłoszenia
        /// </summary>
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfAnnouncement { get; set; }
        /// <summary>
        /// Promień odleglości w jakiej przedmiot mógłbyć zgubiony
        /// </summary>
        public int? Range { get; set; }
        #region Relations
        public virtual Type Type { get; set; }
        public virtual Category Category { get; set; }
        public virtual Status Status { get; set; }
        public virtual ApplicationUser User { get; set; }
        #endregion Relations
        #region ForeignKeys
        [Required]
        [ForeignKey("Type")]
        public int TypeId { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [Required]
        [ForeignKey("Status")]
        public int StatusId { get; set; }


        [Required]
        [ForeignKey("User")]
        public string UserId { get; set; }

        #endregion ForeignKeys
    }
}