﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ThingFinder.Models
{
    public class Color
    {
        public int Id { get; set; }
        /// <summary>
        /// Nazwa Koloru
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        [Display(Name = "Kolor")]
        public string Name { get; set; }
        #region Relation
        public virtual ICollection<Filter> Filter { get; set; }
        #endregion Relation
    }
}