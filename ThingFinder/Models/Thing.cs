﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;

namespace ThingFinder.Models
{
    public class Thing
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public int Id { get; set; }

        /// <summary>
        /// Kod przedmiotu
        /// </summary>
        [Required]
        [Display(Name ="Kod")]
        [MaxLength(9)]
        public string Code { get; set; }

        /// <summary>
        /// Data Rejestracji
        /// </summary>
        [Required]
        [Display(Name ="Data Rejestracji")]
        public DateTime DateRegistration { get; set; }

        /// <summary>
        /// Opis przedmiotu
        /// </summary>
        [Required]
        [Display(Name ="Opis")]
        [MaxLength(70)]
        public string Description { get; set; }

        public Thing()
        {
        }

        public Thing(string Code, string UserId, int StatusId,int categoryId, string description)
        {
            this.Code = Code;
            this.UserId = UserId;
            this.StatusId = StatusId;
            this.DateRegistration = DateTime.Now;
            this.CategoryId = categoryId;
            this.Description = description;
        }

        public Thing(string Code, string UserId, int StatusId, int colorId, int categoryId, string description, int stuffId) : this(Code, UserId, StatusId,categoryId,description)
        {
            //Jesli nie ma żadnych filtrów to dodaj
            if (db.Filter.ToList().Count < 1)
            {
                Filter firstFilter = new Filter() { ColorId = colorId, StuffId = stuffId };
                db.Filter.Add(firstFilter);
                db.SaveChangesAsync();
                this.FilterId = firstFilter.Id;
            }
            else
            {
                Filter existFiltr = null;
                try
                {
                   existFiltr = db.Filter.First(x => x.ColorId == colorId && x.StuffId == stuffId);
                }
                catch (Exception)
                {
                    //Sekwenecja nie zawiera żadnych elementów
                }

                if (existFiltr == null)
                {
                    existFiltr = new Filter() { ColorId = colorId, StuffId = stuffId };
                    db.Filter.Add(existFiltr);
                    db.SaveChanges();
                    this.FilterId = existFiltr.Id;
                }
                else
                    this.FilterId = existFiltr.Id;
            }   


        }

        #region Relations

        public virtual ICollection<Photo> Photo { get; set; }
        public virtual ICollection<Report> Report { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Filter Filter { get; set; }
        public virtual Category Category { get; set; }
        public virtual Status Status { get; set; }

        #endregion Relations

        #region ForeignKeys

        [Required]
        [ForeignKey("User")]
        public string UserId { get; set; }

        [Required]
        [ForeignKey("Filter")]
        public int FilterId { get; set; }

        [Required]
        [Display]
        [ForeignKey("Status")]
        public int StatusId { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        #endregion ForeignKeys
    }
}