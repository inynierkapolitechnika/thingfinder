﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ThingFinder.Models;

namespace ThingFinder.App_Start
{

    public class IdentityDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {

        protected override void Seed(ApplicationDbContext context)
        {
            //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            //string[] roleName = new string[] { "Admin", "User", "Authorized" };

            //foreach (string index in roleName)
            //{
            //    if (!RoleManager.RoleExists(index))
            //        RoleManager.Create(new IdentityRole(index));
            //}

            //addUser("admin1@admin.pl", roleName[0], UserManager);
            //addUser("admin2@admin.pl", roleName[0], UserManager);

            //addUser("user1@user.pl", roleName[1], UserManager);
            //addUser("user2@user.pl", roleName[1], UserManager);

            //addUser("user3@user.pl", roleName[2], UserManager);
            //addUser("user4@user.pl", roleName[2], UserManager);

            base.Seed(context);
        }

        private void addUser(string email, string roleName, UserManager<ApplicationUser> userManager)
        {
            string password = "Demo12#";
            var user = new ApplicationUser();
            user.UserName = email;
            user.Email = email;
            //user.UserInfo = new UserInfo() { };

           var createResult = userManager.Create(user, password);
           if (createResult.Succeeded)
                userManager.AddToRole(user.Id, roleName);      
        }
    }
}