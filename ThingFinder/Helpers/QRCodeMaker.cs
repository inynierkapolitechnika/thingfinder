﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace ThingFinder.Helpers
{
    public class QRCodeMaker
    {

        public static Image GenerateSticker(string code, string url)
        {
            MessagingToolkit.QRCode.Codec.QRCodeEncoder encoder = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            var bitmapQR = ThingFinder.Helpers.PhotoTools.ResizeImage(encoder.Encode(url), 100, 100);
            //pictureBox1.Image = ResizeImage(bitmap, 150, 150);
            Image fd = null;
            Image frame = null;
            try
            {
                //string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
                //string filename = Path.Combine(wanted_path, @"Asserts\Podstawka.bmp");
                frame = Image.FromFile(HostingEnvironment.MapPath(@"/Asserts/Podstawka.bmp"));//@"C:\Users\Karol Walczyna\Source\Repos\thingfinder\ThingFinder\Asserts\Podstawka.bmp");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var bitmap = new Bitmap(frame.Width, frame.Height);

            ThingFinder.Helpers.PhotoTools.CreateCanvas(bitmapQR, frame, bitmap,code);

            try
            {
                fd = bitmap; ;
            }
            catch (Exception ex) { }

            return fd;


        }
    }
}