﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ThingFinder.Validation
{
    public class RoleAuthorizedAttribute : AuthorizeAttribute
    {
        public string Message { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }

            var user = httpContext.User;
            if (user.IsInRole("Authorized"))
            {
                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["MessageFromMyAttribute"] = this.Message;
            filterContext.Result = new RedirectResult("~/Manage/Settings");
        }
    }
}