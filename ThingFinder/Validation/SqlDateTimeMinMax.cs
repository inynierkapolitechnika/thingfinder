﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace ThingFinder.Validation
{
    public class SqlDateTimeMinMax : ValidationAttribute
    {
        private DateTime min = (DateTime)SqlDateTime.MinValue;
        private DateTime max = (DateTime)SqlDateTime.MaxValue;
        private string DefaultErrorMessage;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dateTime = value as DateTime?;

            if (dateTime < min || dateTime > max)
            {
                DefaultErrorMessage = "Wartość z przdziału od " + min.ToString("dd-MM-yyyy") + " do " + max.ToString("dd-MM-yyyy");
                return new ValidationResult(this.ErrorMessage ?? DefaultErrorMessage);
            }
            return ValidationResult.Success;
        }
    }
}
