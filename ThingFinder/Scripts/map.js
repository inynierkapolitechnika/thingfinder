﻿var maps = (function () {

    var map;
    var marker;

    var maps = {
        location: true,
        radius : -1,
        uluru : { lat: 51.919438, lng: 19.145136 }
    }

    var _initMap = function () {

        var styledMapType = styleMap();
        var elems = document.getElementById('map');
        var radius;

        if (maps.radius !== -1) {
            radius = maps.radius;
        }
        
        map = new google.maps.Map(elems, {
            zoom: 6,
            scrollwheel: false,
            streetViewControl: false,
            fullscreenControl: true,
            center: maps.uluru,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                mapTypeIds: ['roadmap', 'styled_map']
            }
        });

        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

        placeMarkerAndPanTo(maps.uluru, radius);

        centerEvent();
        radiusEvent();

        map.addListener('click', function (e) {
            placeMarkerAndPanTo(e.latLng);
        });
    }

    function centerEvent() {
        var e = marker.getCenter();
        var longitude = e.lng().toFixed(6).toString().replace('.', ',');
        var latitude = e.lat().toFixed(6).toString().replace('.', ',');
        $("#Longitude").val(longitude);
        $("#Latitude").val(latitude);
    }

    function radiusEvent() {
        var e = marker.getRadius();
        $("#Range").val(parseInt(e));
    }

    function placeMarkerAndPanTo(latLng, radius) {

        pow = typeof radius !== 'undefined' ? radius : 25000000 / Math.pow(map.getZoom(), 4);

        if (marker) {
            marker.setRadius(pow);
            marker.setCenter(latLng);
        } else {
            marker = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 0,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                center: latLng,
                radius: pow,
                map: map,
                editable: true
            });
            marker.addListener('center_changed', centerEvent);
            marker.addListener('radius_changed', radiusEvent);
        }
    }

    var _setRadius = function (radius) {
        maps.radius = radius;
    }

    var _setPosition = function (latitude, longitude) {
        maps.uluru = { lat: latitude, lng: longitude};
    }

    var _setLocation = function (location) {
        maps.location = location;
    }

    $('#shareLocation').on('shown.bs.modal', function () {
        if (navigator.geolocation && maps.location) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                placeMarkerAndPanTo(pos, position.coords.accuracy);
                map.fitBounds(marker.getBounds());
                map.setCenter(pos);
            });
        } else if (maps.location)
            $("#errorLocalization").show();

        var c = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(c);
    });

    function styleMap() {
        return styledMapType = new google.maps.StyledMapType(
            [
              {
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#ebe3cd"
                    }
                  ]
              },
              {
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#523735"
                    }
                  ]
              },
              {
                  "elementType": "labels.text.stroke",
                  "stylers": [
                    {
                        "color": "#f5f1e6"
                    }
                  ]
              },
              {
                  "featureType": "administrative",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                        "color": "#c9b2a6"
                    }
                  ]
              },
              {
                  "featureType": "administrative.country",
                  "stylers": [
                    {
                        "color": "#4e336f"
                    },
                    {
                        "weight": 1
                    }
                  ]
              },
              {
                  "featureType": "administrative.land_parcel",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                        "color": "#dcd2be"
                    }
                  ]
              },
              {
                  "featureType": "administrative.land_parcel",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#ae9e90"
                    }
                  ]
              },
              {
                  "featureType": "administrative.locality",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                        "color": "#61ccbd"
                    }
                  ]
              },
              {
                  "featureType": "administrative.locality",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                        "color": "#61ccbd"
                    }
                  ]
              },
              {
                  "featureType": "administrative.locality",
                  "elementType": "labels.icon",
                  "stylers": [
                    {
                        "color": "#61ccbd"
                    }
                  ]
              },
              {
                  "featureType": "landscape.natural",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#93817c"
                    }
                  ]
              },
              {
                  "featureType": "poi.park",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                        "color": "#a5b076"
                    }
                  ]
              },
              {
                  "featureType": "poi.park",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#447530"
                    }
                  ]
              },
              {
                  "featureType": "road",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#f5f1e6"
                    }
                  ]
              },
              {
                  "featureType": "road.arterial",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#fdfcf8"
                    }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#f8c967"
                    }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                        "color": "#e9bc62"
                    }
                  ]
              },
              {
                  "featureType": "road.highway.controlled_access",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#e98d58"
                    }
                  ]
              },
              {
                  "featureType": "road.highway.controlled_access",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                        "color": "#db8555"
                    }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#806b63"
                    }
                  ]
              },
              {
                  "featureType": "transit.line",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                  ]
              },
              {
                  "featureType": "transit.line",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#8f7d77"
                    }
                  ]
              },
              {
                  "featureType": "transit.line",
                  "elementType": "labels.text.stroke",
                  "stylers": [
                    {
                        "color": "#ebe3cd"
                    }
                  ]
              },
              {
                  "featureType": "transit.station",
                  "elementType": "geometry",
                  "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                        "color": "#b9d3c2"
                    }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                        "color": "#92998d"
                    }
                  ]
              }
            ],
            { name: 'Another' });
    }

    return {
        init: _initMap,
        setRadius: _setRadius,
        setPosition: _setPosition,
        setLocation: _setLocation
    }

})();


